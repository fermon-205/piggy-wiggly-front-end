var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Caller = require('../models/Caller.js');

/* GET ALL callers */
router.get('/', function(req, res, next) {
  Caller.find(function (err, products) {
    if (err) return next(err);

    var n = products.length;
    if (n > 20)
      n = 20

    res.json(products.reverse().slice(0, n));
  });
});

module.exports = router;
