var express = require('express');
var router = express.Router();
var Event = require('../models/Event.js');
var Contact = require('../models/Contact.js');
var MongoClient = require('mongodb').MongoClient;

router.get('/', function(req, res, next) {


Event.find(function (err, products) {
    if (err) return next(err);
    console.log(products);
    res.json(products);
  });
});

router.post('/', function(req, res, next) {
  console.log(req.body);
  var email_val= null;
  var phone_number = null;
  MongoClient.connect("mongodb://localhost/mean-app", function(err, db) {
    if (err) throw err;
    console.log(req.body.event.title);
    var query = { name: req.body.event.title};
    db.collection("contacts").find(query).toArray(function(err, result) {
      if (err) throw err;
      email_val = result[0].email;
      phone_number = result[0].phone_number
      console.log(result);
      console.log("B: " + result[0].email);
      db.close();
      console.log("C: " + email_val);

      Event.create({ start: req.body.event.start,
        end: req.body.event.end,
        title :  req.body.event.title,
        resizable: { beforeStart: true, afterEnd: true },
        draggable: true,
        email: email_val,
        phone_number: phone_number
      });
    });
  });

  console.log("C: " + email_val);

});

/* UPDATE Event */
router.put('/:id', function(req, res, next) {
  console.log('***********router.put**********')
  console.log(req.params.id);
  console.log('*******************************')
  console.log(req.body);
  console.log('*******************************')
  Event.findByIdAndUpdate(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
    console.log(post);
  });
});

/* DELETE Event */
router.delete('/:id', function(req, res, next) {
  Event.findByIdAndRemove(req.params.id, req.body, function (err, post) {
    if (err) return next(err);
    res.json(post);
  });
});

module.exports = router;
