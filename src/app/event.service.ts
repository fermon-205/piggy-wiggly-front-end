import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {CalendarEvent} from "calendar-utils/dist/calendar-utils";

@Injectable()
export class EventService {

  constructor(private http: Http) { }

  saveEvent(data) {
    return new Promise((resolve, reject) => {
      this.http.post('/event', data)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteEvent(id) {
    return new Promise((resolve, reject) => {
      this.http.delete('/event/' + id)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateEvent(id, data) {
    return new Promise((resolve, reject) => {
      this.http.put('/event/' + id, data)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
