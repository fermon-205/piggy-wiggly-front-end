import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ContactService } from './contact.service';

import { AppComponent } from './app.component';
import { ContactComponent } from './contact/contact.component';
import { RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ContactDetailComponent } from './contact-detail/contact-detail.component';
import { ContactCreateComponent } from './contact-create/contact-create.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { CalendarComponent } from './calendar/calendar.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DemoModule} from './demo/module';
import {EventService} from './event.service';
import { CallerService } from './caller.service';
import { CallerComponent } from './caller/caller.component';

const ROUTES = [
  { path: '', redirectTo: 'contacts', pathMatch: 'full' },
  { path: 'contacts', component: ContactComponent },
  { path: 'contact-details/:id', component: ContactDetailComponent },
  { path: 'contact-create', component: ContactCreateComponent },
  { path: 'contact-create', component: ContactCreateComponent },
  { path: 'contact-edit/:id', component: ContactEditComponent },
  { path: 'open-calendar', component: CalendarComponent},
  { path: 'callers', component: CallerComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    ContactDetailComponent,
    ContactCreateComponent,
    ContactEditComponent,
    CalendarComponent,
    CallerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    DemoModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [ContactService, EventService, CallerService, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
