import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from '../contact.service';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  contact = {};

  constructor(private route: ActivatedRoute, private router: Router, private contactService: ContactService) { }

  ngOnInit() {
    this.getContactDetail(this.route.snapshot.params['id']);
  }

  getContactDetail(id) {
    this.contactService.showContact(id).then((res) => {
      this.contact = res;
      console.log(this.contact);
    }, (err) => {
      console.log(err);
    });
  }

  deleteContact(id) {
    this.contactService.deleteContact(id).then((result) => {
      this.router.navigate(['/contacts']);
    }, (err) => {
      console.log(err);
    });
  }

}
