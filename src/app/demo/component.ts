import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef, OnInit
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
import { Subject } from 'rxjs/Subject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent
} from 'angular-calendar';
import {ContactService} from '../contact.service';
import {EventService} from '../event.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  }
};

@Component({
  selector: 'mwl-demo-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['styles.css'],
  templateUrl: 'template.html'
})
export class DemoComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  view = 'month';

  templateContacts: any;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = new Array<CalendarEvent>();

  activeDayIsOpen = true;

  isDataAvailable = false;


  constructor(private modal: NgbModal, private contactService: ContactService, private eventService: EventService) {
  }

  ngOnInit() {
    this.getTemplateContactList();
    this.contactService.getAllEvents().then((res) => {
      console.log('**********5');
      console.log(res);
      res.forEach(function(p, i, a) {
       a[i].start = new Date(a[i].start);
       a[i].end = new Date(a[i].end);
       a[i].color =  colors.yellow;
      });
      this.events = res.slice(0);
      console.log('**********6');
      console.log(this.events);
      console.log('**********7');
      console.log(typeof(this.events));
      console.log('**********8');
      console.log(typeof(res));
      this.refresh.next();
    }, (err) => {
      console.log(err);
    });
  }


  getTemplateContactList() {
    this.contactService.getAllContacts().then((res) => {
      this.templateContacts = res;
      this.refresh.next();
    }, (err) => {
      console.log(err);
    });
  }



  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    /*this.modal.open(this.modalContent, { size: 'lg' });*/
    this.eventService.updateEvent(event._id, event);
  }

  handleEventSave(eventObj: CalendarEvent): void {
    const e = {event : eventObj}
    this.eventService.saveEvent(e).then((result) => {
      const id = result['_id'];
      this.events[this.events.indexOf(eventObj)]._id = id;
    }, (err) => {
      console.log(err);
    });
  }

  handleEventDelete(eventObj: CalendarEvent): void {
    const e = {event : eventObj}
    this.eventService.deleteEvent(eventObj._id);
  }

  addEvent(): void {
    const ce: CalendarEvent = {
      _id: '',
      title: '',
      start: startOfDay(new Date()),
      end: endOfDay(new Date()),
      color: colors.yellow,
      draggable: true,
      resizable: {
        beforeStart: true,
        afterEnd: true
      }
    };
    this.events.push(ce);
    this.refresh.next();
  }


}
