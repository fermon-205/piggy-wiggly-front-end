import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CallerService {

  constructor(private http: Http) { }

  getAllCallers() {
    return new Promise((resolve, reject) => {
      this.http.get('/caller')
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
