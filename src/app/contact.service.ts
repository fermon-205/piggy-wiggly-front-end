import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {CalendarEvent} from "calendar-utils/dist/calendar-utils";

@Injectable()
export class ContactService {

  constructor(private http: Http) { }

  getAllContacts() {
    return new Promise((resolve, reject) => {
      this.http.get('/contact')
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  showContact(id) {
    return new Promise((resolve, reject) => {
      this.http.get('/contact/' + id)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res)
        }, (err) => {
          reject(err);
        });
    });
  }

  saveContact(data) {
    return new Promise((resolve, reject) => {
      this.http.post('/contact', data)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  updateContact(id, data) {
    return new Promise((resolve, reject) => {
      this.http.put('/contact/' + id, data)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  deleteContact(id) {
    return new Promise((resolve, reject) => {
      this.http.delete('/contact/' + id)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getAllEvents(): Promise<CalendarEvent[]> {
    return new Promise<CalendarEvent[]>((resolve, reject) => {
      this.http.get('/event')
        .map(res => res.json())
        .subscribe(res => {
          console.log('********1');
          let target: CalendarEvent[];
          console.log('********2');
          console.log(res);
          target = res;
          console.log('********3');
          console.log(target);
          console.log('********4');
          resolve(target);
        }, (err) => {
          reject(err);
        });
    });
  }
}
