import { Component, OnInit } from '@angular/core';
import { CallerService } from '../caller.service';

@Component({
  selector: 'app-caller',
  templateUrl: './caller.component.html',
  styleUrls: ['./caller.component.css']
})
export class CallerComponent implements OnInit {

  callers: any;

  constructor(private callerService: CallerService) { }

  ngOnInit() {
    this.getCallerList();
  }

  getCallerList() {
    this.callerService.getAllCallers().then((res) => {
      this.callers = res;
    }, (err) => {
      console.log(err);
    });
  }

}
