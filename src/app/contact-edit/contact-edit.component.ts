import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from '../contact.service';


@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {

  contact = {};

  constructor(private contactService: ContactService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.getContact(this.route.snapshot.params['id']);
  }

  getContact(id) {
    this.contactService.showContact(id).then((res) => {
      this.contact = res;
      console.log(this.contact);
    }, (err) => {
      console.log(err);
    });
  }

  updateContact(id) {
    this.contactService.updateContact(id, this.contact).then((result) => {
      let id = result['_id'];
      this.router.navigate(['/contact-details', id]);
    }, (err) => {
      console.log(err);
    });
  }

}
