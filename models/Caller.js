var mongoose = require('mongoose');

var CallerSchema = new mongoose.Schema({
  caller: String,
});

module.exports = mongoose.model('Caller', CallerSchema);
