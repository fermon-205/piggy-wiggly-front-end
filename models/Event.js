var mongoose = require('mongoose');

var EventSchema = new mongoose.Schema({
  start: Date,
  end: Date,
  title: String,
  resizable: {
    beforeStart: Boolean,
    afterEnd: Boolean
  },
  draggable: Boolean,
  email: String,
  phone_number: String
});

module.exports = mongoose.model('Event', EventSchema);

