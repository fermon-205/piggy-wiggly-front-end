var mongoose = require('mongoose');

var ContactSchema = new mongoose.Schema({
  name: String,
  phone_number: String,
  email: String,
});

module.exports = mongoose.model('Contact', ContactSchema);
